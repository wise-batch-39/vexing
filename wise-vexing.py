import numpy as np
board = np.array([
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 2, 0, 2, 0],
    [0, 0, 0, 0, 0],
])

# Define a function to move a block in a given direction
length=[]
def move_block(row, col, direction):
    if direction == 'L' and col > 0 and board[row, col-1] == 0:
        board[row, col-1] = board[row, col]
        board[row, col] = 0
    elif direction == 'R' and col < board.shape[1]-1 and board[row, col+1] == 0:
        board[row, col+1] = board[row, col]
        board[row, col] = 0
    length.append('moves')

# Define a function to update the board after blocks fall
def update_board():
    for col in range(board.shape[1]):
        for row in range(board.shape[0]-1, 0, -1):
            if board[row, col] == 0:
                for i in range(row-1, -1, -1):
                    if board[i, col] != 0:
                        board[row, col] = board[i, col]
                        board[i, col] = 0
                        break
# Define a function to remove groups of identical blocks
def remove_groups():
    for col in range(board.shape[1]):
        for row in range(board.shape[0]):
            if board[row, col] != 0:
                group = set([(row, col)])
                if row < board.shape[0]-1 and board[row+1, col] == board[row, col]:
                    group.add((row+1, col))
                if col < board.shape[1]-1 and board[row, col+1] == board[row, col]:
                    group.add((row, col+1))
                if len(group) > 1:
                    for r, c in group:
                        board[r, c] = 0
# Play the game
while True:
    move = input()
    move = move.split(',')
    row, col, direction = int(move[0]), int(move[1]), move[2]
    move_block(row, col, direction)
    update_board()
    remove_groups()
    if np.count_nonzero(board) == 0:
        print("The minimum length of moves that can be performed: ",length.count('moves'))
        break
    else:
        print("yet to perform!!")
